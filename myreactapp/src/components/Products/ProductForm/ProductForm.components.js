import React, { Component } from 'react'
// import { Link, NavLink } from 'react-router-dom'
import { formatDate } from '../../../utils/dataUtil'
import { SubmitButton } from '../../Common/SubmitButton/SubmitButton.components'
import './ProductForm.components.css'

const defaultForm = {
    name:'',
    description: '',
    category: '',
    color: '',
    brand: '',
    price: '',
    quantity: '',
    // images: ' ',
    status: '',
    tags: ' ',
    warrentyStatus: false,
    warrentyPeriod: '',
    isReturnEligible: false,
    salesDate: '',
    purchasedDate: '',
    manuDate:'',
    expiryDate: '',
    discountedItem: false,
    discountType: '',
    discountValue: ''
}

const validationFields = {
    category: '',
    name: ''
}

export class ProductForm extends Component {
    constructor(){
        super()

        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...validationFields
            },
            filesToUpload: []
        }
    }

    componentDidMount(){
        console.log('this props >>', this.props)
        const { productData } = this.props;
        if(this.props.productData){
            //edit
            this.setState({
                data:{
                    ...defaultForm,
                    ...productData,
                    discountedItem:productData.discount && productData.discount.discountedItem 
                        ? productData.discount.discountedItem 
                        : false,
                    discountType:productData.discount && productData.discount.discountType 
                        ? productData.discount.discountType 
                        : "",
                    discountValue:productData.discount && productData.discount.discountValue 
                        ? productData.discount.discountValue 
                        : "",
                    purchasedDate: productData.purchasedDate && formatDate(productData.purchasedDate, 'YYYY-MM-DD'),
                    expiryDate: productData.expiryDate && formatDate(productData.expiryDate, 'YYYY-MM-DD'),
                    salesDate: productData.salesDate && formatDate(productData.salesDate, 'YYYY-MM-DD'),
                    manuDate: productData.manuDate && formatDate(productData.manuDate, 'YYYY-MM-DD'),
                }
            })
        }
    }

    handleChange = e => {
        let { name, value, type, checked, files } = e.target;

        if(type === 'file'){
            console.log(files)
            const { filesToUpload } = this.state
            filesToUpload.push(files[0]) 

            return this.setState({
                filesToUpload
            })
        }
        if(type === 'checkbox'){
            value = checked;
        }

        this.setState(preState => ({
            data:{
                ...preState.data,
                [name]: value
            }
        }), ()=>{
            // form validateion here
        })
    }

    onSubmit = e => {
        e.preventDefault();
        this.props.submitCallBack(this.state.data, this.state.filesToUpload);
    }

    removeImage = (index) =>{
        const { filesToUpload } = this.state;
        filesToUpload.splice(index,1)
        this.setState({
            filesToUpload
        })
    }

    render() {
//FOR COLOR PALATE SECTION
        let colorPalet = this.props.isEditMode
            ? <div className="circle-container editMode">
                <div className="color-round">
                    <input type="radio" id="four" name="color"  onChange={this.handleChange} value={this.state.data.color} />
                    <label htmlFor="four" className="round-label four"  style={{backgroundColor : this.state.data.color}}></label>
                </div>

                <div className="hover-colorshower">
                    <span className="round-dropdown"> 
                     <i className="fas fa-caret-down down-arrow"></i>
                </span>


                <div className="hover-color">
                    <input type="text" className="input-color" value={this.state.data.color} placeholder="Color" onChange={this.handleChange} name="color" id="color" />
                </div>
                </div>
            </div>

            :  <div className="circle-container">
                <div className="color-round">
                    <input type="radio" id="one" name="color"  onChange={this.handleChange} value="powderblue" />
                    <label htmlFor="one" className="round-label one"></label>
                </div>

                <div className="color-round">
                    <input type="radio" id="two" name="color"  onChange={this.handleChange} value="mistyrose" />
                    <label htmlFor="two" className="round-label two"></label>
                </div>

                <div className="color-round">
                    <input type="radio" id="three" name="color"  onChange={this.handleChange} value="navajowhite" />
                    <label htmlFor="three" className="round-label three"></label>
                </div>

                <div className="color-round">
                    <input type="radio" id="four" name="color"  onChange={this.handleChange}  />
                    <label htmlFor="four" className="round-label four" ></label>
                </div>

                <div className="hover-colorshower" style={{backgroundColor : this.state.data.color}}>
                    <span className="round-dropdown" > 
                        <i className="fas fa-caret-down down-arrow"></i>
                    </span>


                <div className="hover-color">
                <input type="text" className="input-color" value={this.state.data.color} placeholder="Color" onChange={this.handleChange} name="color" id="color" />
                </div>
                </div>
                </div>


        return (
    <>
        <div className="main-container">
                {/* <h2> {this.props.isEditMode ? 'update' : 'Add'} Product </h2>
                <p> Please add necessary details.</p>
                 */}      
                 
            <form onSubmit={this.onSubmit} className='formgroup' noValidate>
                
            <div className="back-container">
                        <span className="back-btn"> 

                            <i className="fas fa-long-arrow-alt-left" />
                        </span>  
                            <h6> Back to products </h6> 
                    </div> 
                <div className="imgSection">
                <label htmlFor="file" onChange={this.handleChange} className="img-uploader">
                    <div >
                        
                            <i className="fas fa-file-upload img-icon"></i>
                        

                        <input type="file" className="file-input" id="file" />
                    </div>
                    
                    </label>

                    {this.state.filesToUpload.length > 0 && 
                        this.state.filesToUpload.map((file,index) =>(
                            <div key={index} className="preview-fragment">
                                <span className="close-button" 
                                onClick={() => this.removeImage(index)}
                                > X 
                                </span>
                            <img className="img-preview" key={index} src={URL.createObjectURL(file)} alt="file.png" />             
                            <br />
                            </div>
                        ))
                    }
                </div>

                <div className="middleForm">

                    {/* <label htmlFor='name'>Name</label> */}
                      <input type="text" className="text-form" value={this.state.data.name} placeholder="Name" onChange={this.handleChange} name="name" id="name" />
            
            <br />

            <div className="big-price-container">
                <div className="price-section">
                    <div className="price-sign">
                        <label htmlFor='price'><strong> Rs </strong></label>
                    </div>
                        <input type="number" placeholder="Price" value={this.state.data.price} className="priceInput" onChange={this.handleChange} name="price" id="price" />
                </div>
            </div>

                    {/* <label htmlFor='description'>Description</label> */}
                      <textarea rows={6} type="text" className="description" value={this.state.data.description} placeholder="Description here" onChange={this.handleChange} name="description" id="description" />

                    {this.props.isEditMode && (
                        <>
                        <label htmlFor='status'>Status</label>
                        <select type="text" className="formInput" placeholder="Status" value={this.state.data.status} onChange={this.handleChange} name="status" id="status">
                            <option value="">Select Status</option>
                            <option value="avaliable">Avaliable</option>
                            <option value="booked">Booked</option>
                            <option value="out_of_stock">Out Of Stock</option>
                            </select>
                        </>
                    )}

                    <input type="text" className="formInput" placeholder="Category" value={this.state.data.category} onChange={this.handleChange} name="category" id="category" />

                    
                
                    <div className="warrentyStatus-div">
                    <div className="round">
                        <input type="checkbox" id="warrentyStatus" onChange={this.handleChange} checked={this.state.data.warrentyStatus} name="warrentyStatus" />
                        <label htmlFor="warrentyStatus"></label>
                    </div>
                        <label htmlFor='warrentyStatus'> &nbsp; Warrenty Status</label>
                        </div>
                    <br />
                    {
                        this.state.data.warrentyStatus && (
                            <>
                            <div className="warrentyperiod-styling">
                                <div className="triangle-shape"></div>
                                {/* <label htmlFor='warrentyPeriod'>Warrenty Period</label> */}
                                <input type="text" value={this.state.data.warrentyPeriod} className="formInput" placeholder="Warrenty Period" onChange={this.handleChange} name="warrentyPeriod" id="warrentyPeriod" />
                            </div>
                            </>
                        )
                    }



                    <label htmlFor='salesDate'>Sales Date</label>
                        <input type="date" className="formInput" value={this.state.data.salesDate} placeholder="Sales Date" onChange={this.handleChange} name="salesDate" id="salesDate" />

                    <label htmlFor='purchasedDate'>Purchased Date</label>
                        <input type="date" className="formInput" placeholder="Purchased Date" value={this.state.data.purchasedDate} onChange={this.handleChange} name="purchasedDate" id="purchasedDate" />

                    <label htmlFor='manuDate'>Manu Date</label>
                        <input type="date" className="formInput" placeholder="Manu Date" value={this.state.data.manuDate} onChange={this.handleChange} name="manuDate" id="manuDate" />

                    <label htmlFor='expiryDate'>Expiry Date</label>
                        <input type="date" className="formInput" placeholder="Expiry Date" value={this.state.data.expiryDate} onChange={this.handleChange} name="expiryDate" id="expiryDate" />

                    <div  className="discounteditem">

                    <div className="round">
                        <input type="checkbox" checked={this.state.data.discountedItem} onChange={this.handleChange} name="discountedItem" id="discountedItem" />
                        <label htmlFor="discountedItem"></label>
                    </div>
                            <label htmlFor='discountedItem'> &nbsp; Discount Item</label>
                    </div>

                    <br /> 

                    {
                        this.state.data.discountedItem && (
                            <>
                        <div className="value">
                            <label htmlFor='discountType'>DiscountType</label>
                            <select type="text" className="formInput" value={this.state.data.discountType} onChange={this.handleChange} name="discountType" id="discountType" >
                                <option disabled value=""> (Select Type) </option>
                                <option value="percentage"> Percentage </option>
                                <option value="quantity"> Quantity </option>
                                <option value="value"> Value </option>
                                </select>
                        
                            <label htmlFor='discountValue'>Discount Value</label>
                            <input type="text" className="formInput" placeholder="Discount Value" value={this.state.data.discountValue} onChange={this.handleChange} name="discountValue" id="discountValue" />
                        </div>
                            </>
                        )
                    }

                    <hr />
                    <div className="button-container">
                    <SubmitButton 
                    isSubmitting={this.props.isSubmitting}
                />                
                    </div>
                </div>

                <div className="sideForm">
                        <div className="sideForm-smallDiv">

                            

                    <label htmlFor='color'>Color</label> <br />

                

                 {colorPalet}


                     
                <br />
                    <label htmlFor='brand'>Brand</label>
                    <input type="text" className="formInput" placeholder="Brand" value={this.state.data.brand} onChange={this.handleChange} name="brand" id="brand" />
                
                <div className="eligibleCheckbox">
                    
                    <div className="round">
                        <input type="checkbox" id="checkbox" name="isReturnEligible" checked={this.state.data.isReturnEligible} onChange={this.handleChange} />
                        <label  htmlFor="checkbox"></label>
                    </div>
                    <label  htmlFor='checkbox'> &nbsp; Is Return Eligible</label>
                </div>

                    <label htmlFor='quantity'>Quantity</label>
                    <input type="number" className="formInput" placeholder="Quantity" value={this.state.data.quantity} onChange={this.handleChange} name="quantity" id="quantity" />
                
                    <label htmlFor='tags'>Tags</label>
                    <input type="text" className="formInput " placeholder="Tags" value={this.state.data.tags} onChange={this.handleChange} name="tags" id="tags" />
                    
                    
                    
                    </div>
                </div>

               

            </form>
        </div>
    </>
        )
    }
}
