const userModel = require('./../modules/users/user.model');
const jwt = require('jsonwebtoken');
const config = require('./../configs');
const authenticate = require('./authenticate');

module.exports = function(req,res,next){
    let token;
    if(req.headers['authorization'])
        token = req.headers['authorization']
    if(req.headers['x-access-token'])
        token = req.headers['x-access-token']
    if(req.query['token'])
        token = req.query['token']
    if(!token)  
        return next({
            msg: 'Auth failed, token not provided',
            status: 401
        }) 

    
     //token exists now validate
     jwt.verify(token, config.JWT_SECRECT, function(err,decoded){
        if(err){
            return next(err);
        }
        userModel.findById(decoded._id, function(err,user){
            if(err){
                next(err);
            }
            if(user.role !== 'admin'){
                next({
                    msg: 'User is not admin',
                    status: 400
                })
            }
            req.user = user;
            console.log('user is >>', req.user);
            next();
        })
    })

}