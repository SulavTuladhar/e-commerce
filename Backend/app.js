const express = require('express');
// const router = express.Router();
const PORT = require('./configs/index').PORT;
const app = express();
const path = require('path');
const cors = require('cors');

// Importing API route
const API_ROUTER = require  ('./api.router');

//Running mongoose as a part of the application
require('./db_init');


// events stuffs
const events = require('events');
// run socket.js as a part of app.js
require('./socket')(app)

//Importing & loading third party middleware
app.use(cors()); //Enable all request
const morgan = require('morgan');
app.use(morgan('dev'))

//Inbuilt middleware
    //urlencoded data
app.use(express.urlencoded({
    extended: true
})); // This middleware adds incoming data into req.body property
    //Json Parser
app.use(express.json());

//load Inbuilt middleware
//serve static file
app.use(express.static('uploads')) // internal server (if we don't give path)
app.use('/files', express.static(path.join(process.cwd(), 'uploads')));

//Importing application level middleware
const { dirname } = require('path');



app.use('/api', API_ROUTER);




//Error handler application level middleware
app.use(function(req,res,next){
    next({
        msg: 'page not found',
        status: 404
    });
})

//Error handling middleware
app.use(function(err, req, res, next){
    res.status(err.status || 400)
    res.json({
        msg: err.msg || err,
        status: err.status || 400
    })
})



app.listen(PORT, function(err,done){

    if(err){
        console.log('Server listening failed', err)
    }else{
        console.log('server is listening at port' + ' ' + PORT);
    }
})