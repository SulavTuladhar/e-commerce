import React from 'react';
import { Link } from 'react-router-dom';
import { notify } from './../../../utils/toastr';
import { handleError } from '../../../utils/errorHandler';
import { httpClient } from '../../../utils/httpClient';
import './Register.components.css'
import { SubmitButton } from '../../Common/SubmitButton/SubmitButton.components';


const defaultForm = {
    name: '',
    username: '',
    password: '',
    confirmPassword: '',
    email: '',
    phoneNumber: '',
    dob: '',
    gender: '',
    temporaryAddress: '',
    permanentAddress: '',
    role: ''
}

const errorFields = {
    username: false,
    password: false,
    email: false,
    confirmPassword: false
}

export class Register extends React.Component {
    constructor(){
        super();
        this.state = {
            data:{
                ...defaultForm
            },
            error:{
                ...errorFields
            },
            isValidForm: false,
            isSubmitting: false,
        }
        console.log('Constructor at first');
    }

    //init
    componentDidMount(){
        // IT will be invloked onlu once
        console.log('I will be self invoked once component is fully loaded');
        // purpose : data prepration
        // API Call 
        // State modification

    }

    // //Update Stage
    // // Once props or state is modfied component will be update
    // componentDidUpdate(areProps, preState){
    //     // console.log("once state or props is uchange");
    //     console.log('Previos state >>', preState.data)
    //     console.log('Previos state >>', this.state.data)
    //     // check difference and perform necesssary actions
    // }

    //Destroy
    componentWillUnmount(){
        console.log("Once usage is over")
    }

    handleSubmit= e => {
        e.preventDefault();
       

        const isValidForm = this.validateRequiredFields();
        if(!isValidForm){
            return;
        }

        this.setState({
            isSubmitting: true
        })

        httpClient
        .POST(`/auth/register`, this.state.data)
        .then(response =>{
            notify.showInfo("Registration Successfull! please login")
            this.props.history.push('/')
        })
        .catch(err =>{
            handleError(err);
            this.setState({
                isSubmitting: false
            })
        })
    }

    onChange = e => {
        let { name,value } = e.target;
        this.setState(preState => ({
            data:{
                ...preState.data,
                [name]: value
            }
        }),
        ()=>{
            this.validateForm(name) 
            // console.log("username is >>", this.state.data)
        })
        
    }

    validateForm = (fieldName) => {
        let errMsg;
        switch(fieldName){
            case 'username':
                errMsg = this.state.data[fieldName]
                    ? '' 
                    : 'required field*';
                break;

            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['confirmPassword']
                        ? this.state.data['confirmPassword'] === this.state.data[fieldName]
                            ?''
                            :'password didn\'t match*'
                    :   this.state.data[fieldName].length > 6
                            ?''
                            : 'weak password*'
                    : 'required field*'
                break;

            case 'confirmPassword':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['password']
                        ? this.state.data['password'] === this.state.data[fieldName]
                            ? ''
                            :'password didn\'t match*'
                        :this.state.data[fieldName].length > 6
                            ? ''
                            : 'weak password*'
                    :'required field*'
                break;

            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                        ?   ''
                        :   'invalid email*'
                    :'required field*'
                break;


            default:
            break;
        }
        this.setState(preState =>({
            error:{
                ...preState.error,
                [fieldName]:errMsg
            }
        }), () =>{
            const errors = Object
                .values(this.state.error)
                .filter(err => err);
            console.log('Error is >> ', errors)
            console.log('DAta >> ', this.state.data)

            this.setState({
                isValidForm: errors.length === 0
            })
        }
        );
    };

    validateRequiredFields = () => {
        let validForm = true;
        let usernameErr = false;
        let passwordErr = false;
        let confirmPasswordErr = false;
        let emailErr = false;

        if(!this.state.data.username){
            validForm = false;
            usernameErr = 'required field*'
        }
        if(!this.state.data.password){
            validForm = false;
            passwordErr = 'required field*'
        }
        if(!this.state.data.confirmPassword){
            validForm = false;
            confirmPasswordErr = 'required field*'
        }
        if(!this.state.data.email){
            validForm = false;
            emailErr = 'required field*'
        }

        this.setState({
            error:{
                username: usernameErr,
                password: passwordErr,
                confirmPassword : confirmPasswordErr,
                email : emailErr
            }
        })
        return validForm;
    }


    render(){
        return(
            <div className="container">
                <h2>Reigister</h2>
                <p> Please provide necessary details to register</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label htmlFor="name"> Name </label>
                    <input type="text" className="form-control" name= "name" placeholder= "Name" onChange={this.onChange} id="name" />
                    
                    <label htmlFor="username"> Username </label>
                    <input type="text" className="form-control" autoComplete="username" name= "username" placeholder= "Username" onChange={this.onChange} id="username" />
                    <p className="error"> {this.state.error.username && this.state.error.username} </p>

                    <label htmlFor="password"> Password </label>
                    <input type="password" className="form-control" name= "password" autoComplete="new-password" placeholder= "Password" onChange={this.onChange} id="password" />
                    <p className="error"> {this.state.error.password && this.state.error.password} </p>

                    <label htmlFor="confirm-password"> Confirm Password </label>
                    <input type="password" className="form-control" name= "confirmPassword" autoComplete="new-password" placeholder= "Confirm Password" onChange={this.onChange} id="confirm-password" />
                    <p className="error"> {this.state.error.confirmPassword && this.state.error.confirmPassword} </p>

                    <label htmlFor="email"> Email </label>
                    <input type="text" className="form-control" name= "email" placeholder= "Email" onChange={this.onChange} id="email" />
                    <p className="error"> {this.state.error.email && this.state.error.email} </p>
                    <label htmlFor="phoneNumber"> Phone Number </label>
                    <input type="number" className="form-control" name= "phoneNumber" placeholder= "Phone Number" onChange={this.onChange} id="phoneNumber" />
                    
                    <label htmlFor="Gender"> Gender </label>
                    <br />
                    <input type="radio" value="male" name= "gender" onChange={this.onChange} /> Male
                    <input type="radio" value="female" name= "gender" onChange={this.onChange} /> Female
                    <input type="radio" value="Others" name= "gender" onChange={this.onChange} /> Others
                    <br />

                    <label htmlFor="D.O.B"> D.O.B </label>
                    <input type="date" className="form-control" name= "dob" onChange={this.onChange} id="D.O.B" />
                    
                    <label htmlFor="TemporaryAddress"> Temporary Address </label>
                    <input type="text" className="form-control" name= "temporaryAddress" placeholder= "Temporary Address" onChange={this.onChange} id="TemporaryAddress" />
                    
                    <label htmlFor="PermanentAddress"> Permanent Address </label>
                    <input type="text" className="form-control" name= "permanentAddress" placeholder= "Permanent Address" onChange={this.onChange} id="PermanentAddress" />
            
                    <label htmlFor="role">You are here for </label>  &nbsp;
                    <select id="role" name="role" onChange={this.onChange}>
                     <option value="blank" name="role" className="form-control"></option> 
                     <option value="buyer" className="form-control">Buy</option>
                     <option value="seller" className="form-control">Sell</option>
                    </select>

                    <br />
                <SubmitButton
                isSubmitting={this.state.isSubmitting}
                isDisabled = {!this.state.isValidForm}
                ></SubmitButton>
                </form>
                <p>Alrleady Registered? <Link to="/">Back to Login</Link></p>
            </div>
        )
    }
}

/* 
component life cycle stage

init
    -constructor
    -Render
    method - componentDidMount();

change
    -Either state change or props change 
    method - componentDidUpdate();

destroy
    -component life cycle is over
    method - componentWillUnmount();
*/