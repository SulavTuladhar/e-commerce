import React, { Component } from 'react'
import { handleError } from '../../../utils/errorHandler'
import { httpClient } from '../../../utils/httpClient'
import { notify } from '../../../utils/toastr'
import { ProductForm } from '../ProductForm/ProductForm.components'
import './AddProduct.components.css'

export class AddProduct extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             isSubmitting: false
        }
    }

    add = (data, files) =>{
        console.log('function in add product component', data);
        this.setState({
            isSubmitting: true
        })
        const requestData = data;
        if(!requestData.discountedItem){
            delete requestData.discountType 
            delete  requestData.discountValue 
        }
        if(!requestData.warrentyStatus){
            delete requestData.warrentyPeriod 
        }
        httpClient.POST('/product', requestData ,files)
            .then(response =>{
                notify.showSucuess('Product added successfully');
                this.props.history.push('/view_products')
            })
            .catch(err=>{
                handleError(err);
            })
    }

    render() {
        return (

                <div className="big-container">
            <div className="side-content">

            </div>

            <div className='form'>

            <ProductForm
            isSubmitting={this.state.isSubmitting}
            isEditMode = {false}
            submitCallBack={this.add}
            back = {this.props}            
            ></ProductForm>

            </div>
            </div>
        )
    }
}
