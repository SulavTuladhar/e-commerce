// Socket related stuffs
const socket = require('socket.io');
const config = require('./configs');
var users = [];

module.exports = function(app){

    const io = socket(app.listen(config.SOCKET_PORT),{
        cors:{
            accept: '/*'
        }
    });

    io.on('connection', function(client){
        client.emit('hi', 'hi from server');
        var id = client.id;
        client.on('new-user', function(username){
            users.push({
                id: id,
                name : username
            })
            client.emit('users', users);
            client.broadcast.emit('users', users);
        })
        // .emit() for self connected client
        // .broadcast.emit() other then self one
        //  .boradcast.to(socket id).emit() for specific client (private chat)
        
        client.on('new-message', function(data){
            client.emit('replay-message-own', data);
            client.broadcast.to(data.receiverId).emit('replay-message', data)
            console.log('new-msg>>', data)
        })


        client.on('disconnect', function(){
            users.forEach(function(user,index){
                if(user.id === id){
                    users.splice(index,1);
                }
            })
            client.broadcast.emit('users', users); 
        })
    })
}