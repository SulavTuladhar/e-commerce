import React from 'react'

export const SubmitButton =(props) => {
    const disabledLabel = props.disabledLabel || 'submitting...';
    const enabledLabel = props.enabledLabel || 'submit';
    
    let btn = props.isSubmitting
        ? <button disabled className="button"> {disabledLabel} </button>
        : <button type="submit" disabled={props.isDisabled} className="button"> {enabledLabel} </button>

    return btn;
}
