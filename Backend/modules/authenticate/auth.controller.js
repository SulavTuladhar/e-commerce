const express = require('express');
const { ReplSet } = require('mongodb');
const userModel = require("./../users/user.model");
const MAP_USER_REQ = require('../../helpers/map_user_request');
const uploader  = require('../../middlewares/uploader');
const jwt = require('jsonwebtoken');
const configs = require('../../configs');

//JS DOCS commenting style
/**
 * Create token with given data
 * @param {object} data 
 * @returns string
 */
function createToken(data){
    let token = jwt.sign({
        _id: data._id
    },configs.JWT_SECRECT);
    return token
}

//Calling password hashing libary
const passwordHash = require('password-hash');


const router = express.Router();

router.get('/login', function(req,res,next){
    res.render('login.pug');
})

router.post('/login', function(req,res,next){   
    userModel.findOne({
        username: req.body.username
    })
        .then(function(user){
            // console.log('User is >>', user)
            if(!user){
                return next({
                    msg: 'Invalid Username',
                    status: 400
                })
            }
            if(user.status !== 'active'){
                return next({
                    msg: 'You are account is disabled please contact system adminstrator for support',
                    status: 401
                })
            }
            // if(user){
                //password verification
                var isMatched = passwordHash.verify(req.body.password, user.password);
                if(isMatched){
                    // token generation
                    var token = createToken(user);
                    res.json({
                        user: user,
                        token: token
                    });
                }else{
                    next({
                        msg: 'Invalid password',
                        status: 400
                    })
                }

                //complete req-res cycle
            // }
            
        })
        .catch(function(user){
            next(err);
        })

});

router.get('/register', function(req,res,next){
    res.render('register.pug');
})

router.post('/register', uploader.single('image'), function(req,res,next){
    // console.log("Req.body >>", req.body);
    // console.log("req file>>", req.file);
    if(req.fileTypeError){
        return next({
            msg: 'Invalid File format',
            status: 400
        })
    }
    if(req.file){
        req.body.image = req.file.filename;
    }
    //if there is value in req.file, then our file is uploaded in server
    //new USer is mongoose object

    /// CREATE
    var newUser = new userModel({});
    
    var newMappedUser = MAP_USER_REQ(newUser, req.body);
    newMappedUser.password = passwordHash.generate(req.body.password);
    // console.log('newUser >> ', newUser);
    //new User
    newMappedUser.save(function(err,done){
        if(err){
            return next(err);
        }
        res.json(done);
    })
});

module.exports = router