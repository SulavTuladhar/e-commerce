const router = require('express').Router();

//Importing routing level middleware
const AuthRouter = require('./modules/authenticate/auth.controller')
const UserRouter = require('./modules/users/user.controller');
const ProductRouter = require('./modules/product/product.router');
//Importing Appplication level Middleware
const authenticate = require('./middlewares/authenticate');
const authorize = require('./middlewares/authorize');

router.use('/auth', AuthRouter);
router.use('/user', authenticate, UserRouter);
router.use('/product', ProductRouter);
// router.use('/product', Product)

module.exports = router;