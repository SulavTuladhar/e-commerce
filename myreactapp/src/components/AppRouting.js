import React from 'react';
import {BrowserRouter , Route , Switch, Redirect} from 'react-router-dom';
import { Login } from './Auth/Login/Login.components';
import { Register } from './Auth/Register/Register.components';
import { Header } from './Common/Header/Header.components';
// import { Sidebar } from './Common/Sidebar/Siderbar.components';
import { AddProduct } from './Products/AddProduct/AddProduct.components';
import EditProduct from './Products/EditProduct/EditProduct.components';
import { ViewProducts } from './Products/ViewProducts/ViewProducts.components';
import { MessagesComponent } from './Users/Messages/Messages.components';


const Home = (props) => {
    console.log("Props in Home,", props);
    return(
        <h1> Home page </h1>
    )
}


const Dashboard = (props) => {
    console.log("Props in Dashboard,", props);
    return(
        <h1> Dashboard page </h1>
    )
}


const About = (props) => {
    console.log("Props in About,", props);
    return(
        <h1> About page </h1>
    )
}


const Settings = (props) => {
    console.log("Props in Settings,", props);
    return(
        <h1> Settings page </h1>
    )
}

const notFound = (props) =>{
    return(
        <div>
            <h1> Page Not found </h1>
            <img src="./images/notfound.jpg" alt="notfound.jpg"></img>
            </div>
    )
}

const ProtectedRoute = ({component: Component, ...rest}) => {
    return <Route {...rest} render = {routerProps => (
        localStorage.getItem('token')
            ?   <>
            <div className='side-bar'>
            <Header isLoggedIn={true}></Header>
            </div>
            <div className="main-content">
                <Component {...routerProps} />
            </div>
            </>
            : <Redirect to='/'></Redirect> 
    )} />
}

const PublicRoute = ({component: Component, ...rest}) => {
    return <Route {...rest} render = {routerProps => (
            <>
            <Header isLoggedIn={localStorage.getItem('token') ? true : false}></Header>
            <div className="main-content">
                <Component {...routerProps} />
            </div>
            </> 
    )} />
}


export const AppRouting = (props) => {
    return(
        <BrowserRouter>
            <Switch>
            <PublicRoute path="/" exact component={Login} />
            <PublicRoute path="/register" component={Register} />
            <ProtectedRoute path="/about" component={About} />
            <ProtectedRoute path="/home" component={Home} />
            <ProtectedRoute path="/settings" component={Settings} />
            <ProtectedRoute path="/dashboard" component={Dashboard} />
            <ProtectedRoute path="/messages" component={MessagesComponent} />
            <ProtectedRoute path="/add_product" component={AddProduct} />
            <ProtectedRoute path="/view_products" component={ViewProducts} />
            <ProtectedRoute path="/edit_product/:id" component={EditProduct} />
            <PublicRoute component={notFound} />
            </Switch>
        </BrowserRouter>
    )
}