const router = require('express').Router();
const userModel = require('./user.model');
const MAP_USER_REQ = require('../../helpers/map_user_request')
const uploader = require('../../middlewares/uploader');
const fs = require('fs');
const path = require('path');

router.route('/')
    .get(function(req,res,next){    
        var condition = {};
        userModel.find(condition)
            .sort({
                _id: -1
            })
            // .limit(2)
            .exec(function(err,users){
                if(err){
                    return next(err)
                }
                res.json(users)
            });
    })
    .post(function(req,res,next){

    });

router.route('/search')
    .get(function(req,res,next){
        res.send('From search of user');
    })
    .post();
    

router.route('/:id')
    .get(function(req,res,next){

        const id = req.params.id;
        // userModel.findOne({
        //     _id:id
        // })
        //     .then(function(user){
        //         if(!user){
        //             return next({
        //                 msg: 'User not found',
        //                 status: 404
        //             })
        //         }
        //         res.json(user);
        //     })

        userModel.findById(id, function(err,user){
            if(err){
                return next(err);
            }
            if(!user){
                return next({
                    msg: 'User not found',
                    status: 404
                })
            }
            res.json(user)
        })

    })

    .put(uploader.single('image'), function(req,res,next){
        if(req.fileTypeError){
            return next({
                msg: 'Invalid file format',
                status: 400
            })
        }
        const data = req.body;
        if(req.file){
            data.image = req.file.filename;
        }
        // userModel.findByIdAndUpdate
        userModel.findById(req.params.id, function(err,user){
            if(err){
                return next(err);
            }
            if(!user){
                return next({
                    msg: 'User not found',
                    status: 404
                })
            }
            var oldImage = user.image;
            // user exists now update
            // user is mongoose object
            // user.name = req.body.name;
            // console.log('name >>', req.body)
            
            var mappedUpdatedUser =MAP_USER_REQ(user,data);
            mappedUpdatedUser.updated_by = req.user.name;
            mappedUpdatedUser.save(function(err,updated){
                if(err){
                    return next(err);
                }
                if(req.file){
                    fs.unlink(path.join(process.cwd(),'uploads/images/' + oldImage), function(err,done){
                        if(err){
                            console.log('Error in removing old image>>', err)
                        }else{
                            console.log("Sucuess in removing old imgae")
                        }
                    })
                }
                res.json(updated);
            })
        })
        
        

    })

    .delete(function(req,res,next){
        // userModel.findByIdAndRemove
        userModel.findById(req.params.id, function(err,user){
            if(err){
                return next(err);
            }
            if(!user){
                return next({
                    msg: 'User not found',
                    status: 404
                })
            }
            user.remove(function(err,removed){
                if(err){
                    return next(err);
                }
                res.json(removed);
            })
        })
    })

    

module.exports = router;