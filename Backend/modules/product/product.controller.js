const productModel = require('./product.model');

function map_product_req(product, productData){
    if(productData.name)
        product.name = productData.name;
    if(productData.description)
        product.description = productData.description;
    if(productData.category)
        product.category = productData.category;
    if(productData.color)
        product.color = typeof(productData.color) === 'string'
            ? productData.color.split(',')
            : productData.color
    if(productData.brand)
        product.brand = productData.brand;
    if(productData.price)
        product.price = productData.price;
    if(productData.quantity)
        product.quantity = productData.quantity;
    if(productData.images)
        product.images = productData.images;
    if(productData.status)
        product.status = productData.status;
    if(productData.tags)
        product.tags = typeof(productData.tags) === 'string'
            ? productData.tags.split(',')
            : productData.tags
    if(productData.vendor)
        product.vendor = productData.vendor;
    if(productData.warrentyStatus)
    // TODO handel boolean
        product.warrentyStatus = productData.warrentyStatus;
    if(productData.warrentyPeriod)
        product.warrentyPeriod = productData.warrentyPeriod;
        // NOTE flag must be in boolean
    if(productData.isReturnEligible == true || productData.isReturnEligible == false)
        product.isReturnEligible = productData.isReturnEligible
    if(productData.salesDate)
        product.salesDate = productData.salesDate;
    if(productData.purchasedDate)
        product.purchasedDate = productData.purchasedDate;
    if(productData.manuDate)
        product.manuDate = productData.manuDate;
    if(productData.expiryDate)
        product.expiryDate = productData.expiryDate;
    if(!product.discount)
        product.discount = {};
        // TODO handle boolean
    if(productData.discountedItem)
        product.discount.discountedItem = productData.discountedItem;
    if(productData.discountType)
        product.discount.discountType = productData.discountType;
    if(productData.discountValue)
        product.discount.discountValue = productData.discountValue;
    }

function insert(req,res,next){
    //parse incomming data
    //validate data
    //prepare data
    //db operaiton
    //req-res complete
    const data = req.body;

    if(req.fileTypeError){
        return next({
            msg: 'Invalid File Format',
            status: 400
        })
    }
    //prepare data 
    data.vendor = req.user._id;
    if(req.files){
        data.images = req.files.map(function(item,index){
            return item.filename;
        })
    }
    // console.log('Data is >>', data)
    const newProduct = new productModel({});
    // newProduct is mongoose object
    map_product_req(newProduct, data)
    newProduct
        .save()
        .then(function(saved){
            res.json(saved);
        })
        .catch(function(err){
            next(err)
        })

}

function find(req,res,next){
    var condition = {};
    if(req.user.role !== 'admin'){
        condition.vendor = req.user._id;
    }
    // projection
    productModel.find(condition)
    .populate('vendor', {
        username: 1,
        email: 1
    })
    .populate('reviews.user', {
        username: 1
    })
        .exec(function(err,products){
            if(err){
                return next(err);;
            }
                res.json(products);
        })
}

function findById(req,res,next){
    productModel.findById(req.params.id, function(err,product){
        if(err){
            return next(err);
        }
        if(!product){
            return next({
                msg: 'Product not found',
                status: 404
            })
        }
        res.json(product)
    })
}

function update(req,res,next){
    productModel.findById(req.params.id, function(err,product){
        if(err){
            return next(err)
        }
        if(!product){
            return next({
                msg: 'Product not found',
                status: 404
            })
        }
         //product found now update
         if(req.fileTypeError){
             return next({
                 msg: 'Invalid file format',
                 status: 400
             })
         }
         const data = req.body;
         if(req.files){
             data.images = req.files.map(function(item, index){
                 return item.filename;
             });
         }
         //data preparation
         map_product_req(product, data)
        //  if(data.reviewPoint && data.reviewMessage){
        //     var reviewData = {}
        //     if(data.reviewPoint)
        //         reviewData.point = data.reviewPoint
        //     if(data.reviewMessage)
        //         reviewData.message = data.reviewMessage
        //     reviewData.user = req.user._id
        // }
        // product.reviews.push(reviewData);

         product.save(function(err,updated){
             if(err){
                 return next(err)
             }
             //TODO remove existing images from server
             res.json(updated)
         })
    })
}

function remove(req,res,next){
    productModel.findById(req.params.id, function(err,product){
        if(err){
            return next(err)
        }
        if(!product){
            return next({
                msg: 'Product not found',
                status: 404
            })
        }
        product.remove(function(err,removed){
            if(err){
                return next(err)
            }
            res.json(removed)
        })
    })
}

function addReview(req,res,next){
    productModel.findById(req.params.productId, function(err,product){
        if(err){
            return next(err)
        }
        if(!product){
            return next({
                msg: 'Product not found',
                status: 404
            })
        }
         //product found now update
         const data = req.body;
         //data preparation
        if(data.reviewPoint && data.reviewMessage){
            var reviewData = {}
            if(data.reviewPoint)
                reviewData.point = data.reviewPoint
            if(data.reviewMessage)
                reviewData.message = data.reviewMessage
            reviewData.user = req.user._id;
        
        product.reviews.push(reviewData);
        }
        else{
            return next({
                msg: 'MIssing required Parameters',
                status: 400
            })
        }
         product.save(function(err,updated){
             if(err){
                 return next(err)
             }
             res.json(updated)
         })
    })

}

function search(req,res,next){
    // TODO
    var condition = {};
    productModel.find(condition)
        .exec(function(err,products){
            if(err){
                return next(err);;
            }
                res.json(products);
        })

}

module.exports = {
    insert,
    find,
    findById,
    update,
    remove,
    addReview,
    search
}